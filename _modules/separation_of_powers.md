---
layout: module
title: Separation of Powers
permalink: /modules/separation_of_powers/
summary: Different aspects of governance fall under the purview of distinct and countervailing entities.
type: structure
---

Separation of powers is a mechanism of checks and balances used in governance to divide decision-making abilities among separate groups. It is used to ensure that no one body or individual has autocratic rule; each group has different responsibilities that will not overreach into those of another. 
Though separation of powers is frequently associated with the three branch system, such as that used in the U.S. government, some implementations may split power between only two bodies.

**Input:** a democratic system with potential for diffusion of power; political actors or bodies with distinct roles and responsibilities

**Output:** system of governance with at least two political bodies to keep decision-making dissipated and protect democracy

## Background

John Locke was one of the earliest thinkers to voice the importance of a separation of powers. He proposed that governance be divided between a king and parliament, rather than resting solely in the hands of one or the other. Most frequently known for coining the term, however, is the French Enlightenment figure Baron de Montesquieu, who analyzed the benefits of the three-pronged, or tripartite, system of legislative, judiciary, and executive branches in “The Spirit of Laws” (1748). 

While Locke and Montesquieu are framed as the preliminary thinkers in this area, John Calvin advocated for a system akin to a separation of powers two hundred years before. Even Aristotle’s “Politics” mentions a “mixed” government with power spanning between many different political bodies. 

## Feedback loops

### Sensitivities

* Prevents consolidation and exploitation of power
* Holds accountable and responsible each branch
* Increases likelihood of a fair, just government exempt from prejudicial and biased lawmaking

### Oversights

* Potentially complicates and slows processes of decision-making
* It can result in a lack of action if the branches cannot reach a consensus due to varying majority beliefs
* Without proper definition of roles, tasks can fall through the cracks and perpetually be seen as the responsibility of a different branch

## Implementations

### Communities

The tripartite system is utilized in countries around the world including the U.S., the Czech Republic, and France. Not every system looks the same; for example, Malaysia has a parliament as legislative branch, prime minister, cabinet, government and civil service departments as executive, and federal and lower courts as judiciary while the Republic of China has five branches distinct branches.

In smaller scale governance systems, such as those of companies and non-profits, the separation of powers often takes the form of separate roles for the board, CEO, director, and staff. 

### Tools

* The [Carver Policy Governance]( https://www.carvergovernance.com/pg-np.htm) model is based on principles of the separation of powers and can be a useful reference for creating a governance system.
* The Cullinane Law Group offers an example of the [breakdown of powers]( https://cullinanelaw.com/nonprofit-board-vs-executive-director/) in a nonprofit, though the ideas in this article can be more widely applied to other companies and organizations.

## Further resources

Nazir, M., Ahmad, T., & Khan Afghani, M. A. (2017). Separation of Power: A Comparative Analysis. Commonwealth Law Review Journal, 3.
Barendt, E. (2017). Separation of powers and constitutional government. In The Rule of Law and the Separation of Powers(pp. 275-295). Routledge.
Fairlie, J. (1923). The Separation of Powers. Michigan Law Review, 21(4), 393-436. doi:10.2307/1277683
