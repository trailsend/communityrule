---
layout: module
title: Power Vacuum
permalink: /modules/power_vacuum/
summary: A point of authority at which no structure or role is specified.
type: structure
---
