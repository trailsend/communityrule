---
layout: module
title: Federation
permalink: /modules/federation/
summary: A central authority delegates power to, and is co-governed by, subsidiary authorities.
type: structure
---

Federation is the principle of connecting smaller, partially self-governing units through a central unit that manages shared resources among them and powers over them. It generally seeks to create a multi-layer architecture in which authority lies at the most appropriate level.

Federal*ism* is generally associated with calls for a stronger central authority.

Federation and confederation are sometimes distinguished, wherein federation is a system where sovereignty lies chiefly in the central unit (such as the US federal government), while in a confederation sovereignty is understood to lie with the constiuent units (such as the European Union).

**Input:** constituent units, central unit, contracts among them

**Output:** cohesion, multi-layered authority structure

## Background

Federated structures have been widespread in various forms of government, as well as in private enterprise. Early examples include the pre-Columbian Haudenosaunee Confederation (or Iroquois Confederacy), followed by post-colonial formations in the Americas such as the United States, Mexico, and Brazil.

In the private sector, cooperative businesses frequently form federations, which are typically second-order co-ops (or nonprofit associations) whose constiuent members are smaller cooperatives or non-cooperative businesses.

Federation may be considered an implementation of the philosophical concept of subsidarity, which holds that power should reside as locally as possible to the relevant context.

## Feedback loops

### Sensitivities

* Balance between local governance and economies of scale

### Oversights

* Can exact high costs of governance among autonomous constituent units

## Implementations

### Communities

* Cooperative federations, such as Confcooperative in Italy and US "generation and transmission" electric cooperatives
* Governments in many countries

In fiction:

* [United Federation of Planets](https://memory-alpha.fandom.com/wiki/United_Federation_of_Planets) in the Star Trek franchise

### Tools

* [Fediverse](https://en.wikipedia.org/wiki/Fediverse), a set of interoperable federated social networks using software such as Mastodon, GNU Social, and Pleroma
* [Lightning Network](https://en.wikipedia.org/wiki/Lightning_Network), a scalability solution for blockchain systems that dynamically delegates authority to clusters of trusting nodes

## Further resources

* Ilten, Carla. "[Finding “meso-level” community at SASE 2017](https://organizationaldynamics.wordpress.com/2018/01/10/finding-meso-level-community-at-sase-2017/). 
Center on Organizational Dynamics. January 10, 2018.
